package com.curso.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.xml.sax.ErrorHandler;

public class ErroresHandler {
    @JsonProperty("message")
    private String message;
    @JsonProperty("status_code")
    private int statusCode;
    @JsonProperty("uri")
    private String uriRequested;


    public ErroresHandler(int statusCode,String message, String uriRequested) {
        this.message = message;
        this.statusCode = statusCode;
        this.uriRequested = uriRequested;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getUriRequested() {
        return uriRequested;
    }

    public void setUriRequested(String uriRequested) {
        this.uriRequested = uriRequested;
    }

    public ErroresHandler(Exception  exception, String uriRequested) {
        this.message=exception.getMessage();
        this.statusCode=statusCode;
        this.uriRequested=uriRequested;


    }
}
