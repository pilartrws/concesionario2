package com.curso.demo.model;

public class Validador {
    public static boolean validaMatricula(String matricula){

        return matricula.toUpperCase().matches("^[0-9]{4}[A-Za-z]{3}$");
    }
}
