package com.curso.demo.model;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Cliente {
    @Id
    private Integer id;
    private String name;
    private String direccion;
    private String dni;

    public Cliente() {

    }


    public Cliente (Integer id, String name, String direccion, String dni) throws Exception {
        if (id>0){
            this.id = id;
        }else throw new Exception("La 'id':"+id+" es incorrecta");
        this.name = name;
        this.direccion = direccion;
        this.dni = dni;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }
}
