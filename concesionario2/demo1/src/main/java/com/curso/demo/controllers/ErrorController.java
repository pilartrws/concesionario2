package com.curso.demo.controllers;

import com.curso.demo.model.ErroresHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


@ControllerAdvice
public class ErrorController {
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErroresHandler>methodArgumentNotValidException(HttpServletRequest request,MethodArgumentNotValidException e){
        ErroresHandler erroresHandler=new ErroresHandler(HttpStatus.BAD_REQUEST.value(),e.getMessage(),request.getRequestURI());
        return new ResponseEntity<>(erroresHandler,HttpStatus.BAD_REQUEST);
    }
}