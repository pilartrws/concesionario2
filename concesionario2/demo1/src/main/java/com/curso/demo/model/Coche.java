package com.curso.demo.model;

import java.util.TreeSet;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

//modificar coche,añadir coche,borrar coche,buscar coche
@Entity

public class Coche {
    @Id
    private String matricula;
    private String marca;
    private String modelo;


    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public Coche() {
    }


    public Coche(String marca,String matricula, String modelo)throws Exception {
        if (Validador.validaMatricula(matricula)){
            this.matricula=matricula;
        }else throw new Exception("Matrícula incorrecta: "+ matricula);
        this.marca = marca;
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
}
