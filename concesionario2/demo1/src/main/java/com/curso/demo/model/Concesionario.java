package com.curso.demo.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

public class Concesionario {
    private HashMap <Integer,Cliente> clientes;
    private HashSet<Coche>listaCochesStock=new HashSet<>();
    private HashSet<Coche>listaCochesVendidos=new HashSet<>();
    private HashSet<Coche>listaCochesReparacion=new HashSet<>();

    public void setClientes(HashMap<Integer, Cliente> clientes) {
        this.clientes = clientes;
    }

    public HashSet<Coche> getListaCochesStock() {
        return listaCochesStock;
    }

    public void setListaCochesStock(HashSet<Coche> listaCochesStock) {
        this.listaCochesStock = listaCochesStock;
    }

    public HashSet<Coche> getListaCochesVendidos() {
        return listaCochesVendidos;
    }

    public void setListaCochesVendidos(HashSet<Coche> listaCochesVendidos) {
        this.listaCochesVendidos = listaCochesVendidos;
    }

    public HashSet<Coche> getListaCochesReparacion() {
        return listaCochesReparacion;
    }

    public void setListaCochesReparacion(HashSet<Coche> listaCochesReparacion) {
        this.listaCochesReparacion = listaCochesReparacion;
    }

    public Concesionario(HashMap<Integer, Cliente> clientes, HashSet<Coche> listaCochesStock, HashSet<Coche> listaCochesVendidos, HashSet<Coche> listaCochesReparacion) {
        this.clientes = clientes;
        this.listaCochesStock = listaCochesStock;
        this.listaCochesVendidos = listaCochesVendidos;
        this.listaCochesReparacion = listaCochesReparacion;
    }

    public Concesionario() {
    }
    /*public Cliente getCliente(Integer id){
        return Cliente.getCliente(id);
    }*/

    public String getClientes(){
        Collection<Cliente> retrieve = clientes.values();
        String result = "[";
        result+=clientes;



        result+="]";
        return result;
    }
}
