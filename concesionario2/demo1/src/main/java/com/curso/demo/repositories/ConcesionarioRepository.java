package com.curso.demo.repositories;

import com.curso.demo.model.Coche;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface ConcesionarioRepository extends JpaRepository<Coche, Integer > {
 Optional<List<Coche>> findById(int cocheId);




}
