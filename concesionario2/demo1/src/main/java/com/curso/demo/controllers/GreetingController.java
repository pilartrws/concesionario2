package com.curso.demo.controllers;

import com.curso.demo.model.Cliente;
import com.curso.demo.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GreetingController {

    @Autowired
    private ClienteRepository clienteRepository;

    @RequestMapping("/greeting")
    public String greeting() throws Exception {
        clienteRepository.save(new Cliente(34, "juan", "lalala", "38133174W"));
        return "Enhorabuena, has llegado al final de internet.";
    }


    @GetMapping("/cliente/{id}")
    public String cliente (@PathVariable Integer id){
        return "cliente "+id;
    }

    @GetMapping("/clientes")//funciona
    public List clientes() throws Exception {

        if (clienteRepository.findAll().isEmpty()) {
            throw new Exception("No hay cliente");
        } else {
            return clienteRepository.findAll();
        }
    }

    @PostMapping("cliente/{id}/direccion/{direccion}")
    public String cambiarDireccion(@PathVariable Integer id,@PathVariable String direccion){
        clienteRepository.getOne(id).setDireccion(direccion);
        return "El nombre ha sido cambiado con éxito";
    }
    @PostMapping("cliente/{id}/nombre/{name}")
    public String cambiarNombre(@PathVariable Integer id,@PathVariable String name){
        clienteRepository.getOne(id).setName(name);
        return "El nombre ha sido cambiado con éxito";
    }
    @PostMapping("cliente/{id}/dni/{dni}")
    public String cambiarDni(@PathVariable Integer id,@PathVariable String dni){
        clienteRepository.getOne(id).setDni(dni);
        return "El nombre ha sido cambiado con éxito";
    }
}