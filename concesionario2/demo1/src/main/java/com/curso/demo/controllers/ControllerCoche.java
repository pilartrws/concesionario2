package com.curso.demo.controllers;

import com.curso.demo.model.Coche;
import com.curso.demo.model.Concesionario;
import com.curso.demo.model.EstadoCoche;
import com.curso.demo.model.TipoCoche;
import com.curso.demo.repositories.CocheRepository;
import com.curso.demo.repositories.ConcesionarioRepository;
import javassist.NotFoundException;
import org.hibernate.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
//excepcion valores nulll
//metodos personalizados jpa
// exception handler

@RestController

public class ControllerCoche {
    @Autowired
    private CocheRepository cocheRepository;

    @RequestMapping("/coche")
    public String greeting() throws Exception {
        cocheRepository.save(new Coche("Ferrari","1234N","F50"));
        return "El coche ha sido creado";
    }


    @GetMapping("coche/{matricula}")//funciona
    public String buscarCoche(@PathVariable String matricula) throws Exception {

        Optional<Coche> coches = cocheRepository.findById(matricula);
        if (coches.isPresent()) {
            coches.get();
        } else {

            throw new Exception("el coche con matricula" + matricula + "no existe");
        }
        return "coche encontrado";
    }


    @GetMapping("/coches")//funciona
    public List coches() throws Exception {

        if (cocheRepository.findAll().isEmpty()) {
            throw new Exception("No hay coches");
        } else {
            return cocheRepository.findAll();
        }
    }


    @DeleteMapping("/coche/{matricula}")//funciona
    public String delecteCoche(@PathVariable String matricula) throws Exception {
        Optional<Coche> coche = cocheRepository.findById(matricula);
        if (coche.isPresent()) {
            cocheRepository.deleteById(matricula);
        } else {
            throw new Exception("el coche con matricula" + matricula + " no existe");
        }
        return "borrado";
    }
    @PostMapping("coche/{matricula}/marca/{marca}")
    public String cambiarDireccion(@PathVariable String  matricula,@PathVariable String marca){
        cocheRepository.getOne(matricula).setMarca(marca);
        return "El nombre ha sido cambiado con éxito";
    }
    @PostMapping("coche/{matricula}/modelo/{modelo}")
    public String cambiarNombre(@PathVariable String matricula,@PathVariable String modelo){
        cocheRepository.getOne(matricula).setModelo(modelo);
        return "El nombre ha sido cambiado con éxito";
    }
    @PostMapping("coche/{matricula}/matricula/{matriculaNueva}")
    public String cambiarDni(@PathVariable String matricula,@PathVariable String matriculaNueva){
        cocheRepository.getOne(matricula).setMatricula(matriculaNueva);
        return "El nombre ha sido cambiado con éxito";
    }
/*
    @PostMapping("/coche")//funciona falta meter en listas de concesionario
    public Coche addCoche(@RequestBody String matricula) throws Exception {
        Optional<Coche> coche1 = cocheRepository.findById(matricula.getCocheId());
        if (coche1.isPresent()) {
            throw new Exception("el coche con el id indicado ya existe"
            );
        }
        if (coche.getPrecioCompraVenta() <= 0) {
            throw new Exception("el precio de compraventa no puede ser 0");
        }

        cocheRepository.save(coche);
        return coche;
    }

    @PutMapping("/update")
    public Coche updateIt(@RequestBody Coche coche) throws Exception {
        cocheRepository.save(coche);
        if (coche.getPrecioCompraVenta() <= 0) {
            throw new Exception("el precio de compra-venta no puede ser 0");
        }
        List<Coche>lista=cocheRepository.findAll();
        for(Coche coche1:lista){
            if(coche.getMatricula()==coche1.getMatricula()){throw new Exception("el coche con esa matrícula ya existe");}
        }


        return coche;
    }

*/
}




