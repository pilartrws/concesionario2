package com.curso.demo.model;

public enum EstadoCoche {
    VENDIDO,
    REPARACION,
    ENSTOCK,
    RESERVADO,
}

