package com.curso.demo.repositories;

import com.curso.demo.model.Coche;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface CocheRepository extends JpaRepository<Coche, String > {
 Optional<Coche> findById(String cocheId);


}